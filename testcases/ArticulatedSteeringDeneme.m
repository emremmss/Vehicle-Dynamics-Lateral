%% Articulated Steering Deneme
%
%% Simulation models and parameters
% First, all classes of the package are imported with

import VehicleDynamicsLateral.*
%%
% Choosing simulation time span

T =     15;                  % Total simulation time [s]
resol = 50;                % Resolution
TSPAN = 0:T/resol:T;        % Time span [s]

%%
% Choosing tire and vehicle model. In this case, the parameters are defined by the user.

% Choosing vehicle
VehicleModel = VehicleArticulatedNonlinear(); %for linear model uncommen  this row

% The System is completely defined once we atribute the chosen tire model to the vehicle object.

VehicleModel.tire = TirePacejka();
turnradius = (VehicleModel.a+VehicleModel.b)/sin(18*pi/180);

% steering input front
VehicleModel.deltaf = 18*pi/180.*tanh(2*pi/3*TSPAN);
% 1*pi/180*sin(T^-1*2*pi*TSPAN);

%%
% To define a simulation object (simulator) the arguments must be the vehicle object and the time span. The default parameters of the simulation object can be found in <api/Simulator.html Simulator>.

simulator = Simulator(VehicleModel, TSPAN);
simulator.V0 = 10/3.6; % mps

%% Run simulation
% To simulate the system we run the Simulate method of the simulation object.

simulator.Simulate();
% 
% % Retrieving states
% XT      = simulator.XT;
% YT      = simulator.YT;
% PSI     = simulator.PSI;            % Vehicle yaw angle [rad]
% dPSI    = simulator.dPSI;           % Yaw rate [rad/s]
% PHI     = simulator.PHI;            % Vehicle roll angle [rad]
% dPHI    = simulator.dPHI;           % Roll rate [rad/s]
% VEL     = simulator.VEL;            % Vehicle CG velocity [m/s]
% ALPHAT  = simulator.ALPHAT;         % Vehicle side slip angle [rad]
% a       = simulator.Vehicle.a;      % Distance from front axle of the car (tractor) to the center of mass of the car (tractor) [m]
% b       = simulator.Vehicle.b;      % Distance from center of mass of the car (tractor) to the front axle of the car (tractor) [m]
% c       = simulator.Vehicle.c;      % Distance from rear axle of the tractor to articulation (R-A) [m]
% d       = simulator.Vehicle.d;      % Distance from articulation to semitrailer center of mass (A-M) [m]
% e       = simulator.Vehicle.e;      % Distance from semitrailer center of mass to semitrailer axle (A-M) [m]
% lT      = simulator.Vehicle.wT / 2;   % Distance from articulation to semitrailer axle (A-M) [m]
% 
% ALPHAF = atan2((a * dPSI + VEL.*sin(ALPHAT)),(VEL.*cos(ALPHAT))); % Slip angle @ front axle [rad]
% ALPHAR = atan2((-b * dPSI + VEL.*sin(ALPHAT)),(VEL.*cos(ALPHAT))); % Slip angle @ rear axle [rad]
% ALPHAM = atan2((d + e).*(dPHI - dPSI) + VEL.*sin(ALPHAT + PHI) - dPSI.*cos(PHI)*(b + c), VEL.*cos(ALPHAT + PHI) + dPSI.*sin(PHI).*(b + c));
% 
% % Velocity
% VF = sqrt((VEL.*cos(ALPHAT)).^2 + (a * dPSI + VEL.*sin(ALPHAT)).^2);
% VR = sqrt((VEL.*cos(ALPHAT)).^2 + (-b * dPSI + VEL.*sin(ALPHAT)).^2);
% 
% 
% figure(1)
% % plot(TSPAN,180/pi*VehicleModel.deltaf)
% % xlabel('time [s]')
% % ylabel('Steering angle [deg]')
% plot(XT,YT,'b');
% hold on
% plot(XT,YT);
% axis equal
% grid minor

%% Results
%

g = Graphics(simulator);
g.TractorColor = 'r';
g.SemitrailerColor = 'g';
g.Frame();
% g.Animation();

%%
%
% <<../illustrations/AnimationTemplateArticulated.gif>>
%
%% See Also
%
% <../index.html Home> | <TemplateSimple.html TemplateSimple>
%
