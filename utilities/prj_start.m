function prj_start()

proj = simulinkproject;
proj_root = proj.RootFolder;

myWork          = fullfile(proj_root,'work');
myCahceFolder   = fullfile(myWork,'sim_cache');

if ~exist(myCahceFolder,'dir')
    mkdir(myCahceFolder);
end

addpath(myWork);
cd(myWork);