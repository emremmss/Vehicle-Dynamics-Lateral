function get_simResults(simulator)

% Retrieving states
TSPAN   = simulator.TSpan;

XT      = simulator.XT;
YT      = simulator.YT;
PSI     = simulator.PSI;            % Vehicle yaw angle [rad]
dPSI    = simulator.dPSI;           % Yaw rate [rad/s]
PHI     = simulator.PHI;            % Vehicle roll angle [rad]
dPHI    = simulator.dPHI;           % Roll rate [rad/s]
VEL     = simulator.VEL;            % Vehicle CG velocity [m/s]
ALPHAT  = simulator.ALPHAT;         % Vehicle side slip angle [rad]
a       = simulator.Vehicle.a;      % Distance from front axle of the car (tractor) to the center of mass of the car (tractor) [m]
b       = simulator.Vehicle.b;      % Distance from center of mass of the car (tractor) to the front axle of the car (tractor) [m]
c       = simulator.Vehicle.c;      % Distance from rear axle of the tractor to articulation (R-A) [m]
d       = simulator.Vehicle.d;      % Distance from articulation to semitrailer center of mass (A-M) [m]
e       = simulator.Vehicle.e;      % Distance from semitrailer center of mass to semitrailer axle (A-M) [m]
lT      = simulator.Vehicle.wT / 2;   % Distance from articulation to semitrailer axle (A-M) [m]

ALPHAF = atan2((a * dPSI + VEL.*sin(ALPHAT)),(VEL.*cos(ALPHAT))); % Slip angle @ front axle [rad]
ALPHAR = atan2((-b * dPSI + VEL.*sin(ALPHAT)),(VEL.*cos(ALPHAT))); % Slip angle @ rear axle [rad]
ALPHAM = atan2((d + e).*(dPHI - dPSI) + VEL.*sin(ALPHAT + PHI) - dPSI.*cos(PHI)*(b + c), VEL.*cos(ALPHAT + PHI) + dPSI.*sin(PHI).*(b + c));

% Velocity
VF = sqrt((VEL.*cos(ALPHAT)).^2 + (a * dPSI + VEL.*sin(ALPHAT)).^2);
VR = sqrt((VEL.*cos(ALPHAT)).^2 + (-b * dPSI + VEL.*sin(ALPHAT)).^2);



% CG position tractor
PTO_t = [XT YT];                                            % P_{T/O}

% Relative positions
PFT_t = [ a*cos(PSI)  a*sin(PSI)];                        % P_{F/T}
PRT_t = [-b*cos(PSI) -b*sin(PSI)];                        % P_{R/T}
PAT_t = [-(b+c)*cos(PSI) -(b+c)*sin(PSI)];                % P_{A/T}
PSA_t = [-d*cos(PSI-PHI) -d*sin(PSI-PHI)];            % P_{S/A}
PMA_t = [-(d+e)*cos(PSI-PHI) -(d+e)*sin(PSI-PHI)];   % P_{M/A}

% CG position semitrailer
PSO_t = PSA_t + PAT_t + PTO_t;                                  % P_{S/O}

% Axle position
PFO_t = PFT_t + PTO_t;                                          % P_{F/O}
PRO_t = PRT_t + PTO_t ;                                         % P_{R/O}
PMO_t = PMA_t + PAT_t + PTO_t;                                  % P_{M/O}

figure(1);
% subplot(3,1,1);
% plot(XT,YT);
% axis equal
% grid minor
% subplot(3,1,[2 3]);
plot(PTO_t(:,1),PTO_t(:,2));
hold on;
plot(PSO_t(:,1),PSO_t(:,2));
plot(PFO_t(:,1),PFO_t(:,2));
plot(PRO_t(:,1),PRO_t(:,2));
plot(PMO_t(:,1),PMO_t(:,2));
grid minor
axis equal
legend({'PTO_t','PSO_t','PFO_t','PRO_t','PMO_t'});


end